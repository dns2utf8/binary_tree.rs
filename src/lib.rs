
use std::cmp::Ordering::{self, Less, Greater};
use std::iter::FromIterator;
//use std::mem::swap;


#[derive(Debug, PartialEq)]
pub struct BinaryTree {
    data: isize,
    left: Option<Box<BinaryTree>>,
    right: Option<Box<BinaryTree>>,
}

impl BinaryTree {
    pub fn new(data: isize) -> BinaryTree {
        BinaryTree {
            data,
            left: None,
            right: None,
        }
    }

    fn boxed_new(data: isize) -> Option<Box<Self>> {
        Some(Box::new(BinaryTree::new(data)))
    }

    pub fn insert(&mut self, data: isize) {
        let (field, other, order_outer) = if data < self.data {
            // insert left
            (&mut self.left, &mut self.right, Less)
        } else {
            // insert right
            (&mut self.right, &mut self.left, Greater)
        };
        if let &mut Some(ref mut field) = field {
            if field.should_push_down(&data, order_outer) {
                field.insert(data);
            } else {
                if data.cmp(&field.data) == order_outer && other.is_none() {
                    *other = BinaryTree::boxed_new(self.data);
                    self.data = field.data;
                    field.data = data;
                } else {
                    field.insert(data)
                }
            }
        } else {
            *field = BinaryTree::boxed_new(data);
        }
    }

    fn should_push_down(&self, data: &isize, order_outer: Ordering) -> bool {
        self.data.cmp(data) == order_outer
    }

    pub fn contains(&self, needle: &isize) -> bool {
        if *needle == self.data {
            return true;
        }

        let field = if *needle < self.data {
            &self.left
        } else {
            &self.right
        };
        if let &Some(ref field) = field {
            field.contains(needle)
        } else {
            false
        }
    }

    pub fn iter(&mut self) -> BinaryTreeIterator {
        BinaryTreeIterator {
            state: vec![InTreeState::EnterNode(self)],
        }
    }
}

pub struct BinaryTreeIterator<'a> {
    state: Vec<InTreeState<'a>>,
}

enum InTreeState<'a> {
    EnterNode(&'a BinaryTree),
    ExtractData(&'a BinaryTree),
}

impl<'a> Iterator for BinaryTreeIterator<'a> {
    type Item = isize;

    fn next(&mut self) -> Option<Self::Item> {
        use InTreeState::*;

        if self.state.len() == 0 {
            return None;
        }

        match self.state.pop().expect("handled empty case") {
            EnterNode(root) => {
                if let Some(ref field) = root.right {
                    self.state.push(EnterNode(field));
                }

                self.state.push(ExtractData(root));

                if let Some(ref field) = root.left {
                    self.state.push(EnterNode(field));
                }

                self.next()
            }
            ExtractData(root) => {
                Some(root.data)
            }
        }
    }
}

pub struct BinaryTreeCollection {
    tree: Option<BinaryTree>,
    size_of_tree: usize,
}

impl BinaryTreeCollection {
    pub fn new() -> BinaryTreeCollection {
        BinaryTreeCollection {
            tree: None,
            size_of_tree: 0,
        }
    }

    pub fn insert(&mut self, data: isize) {
        if let Some(ref mut tree) = self.tree {
            tree.insert(data);
        } else {
            self.tree = Some(BinaryTree::new(data));
        }
        self.size_of_tree += 1;
    }

    pub fn iter<'a>(&'a mut self) -> Box<Iterator<Item=isize> + 'a> {
        //if let Some(ref mut tree) = self.tree {
        if let Some(tree) = self.tree.as_mut() {
            Box::new(tree.iter())
        } else {
            Box::new(std::iter::empty())
        }
    }
}

impl FromIterator<isize> for BinaryTreeCollection {
    fn from_iter<I: IntoIterator<Item=isize>>(iter: I) -> Self {
        let mut t = BinaryTreeCollection::new();

        for i in iter {
            t.insert(i);
        }

        t
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn insert_left() {
        let mut t = BinaryTree::new(4);
        t.insert(2);

        let expected = BinaryTree {
            data: 4,
            left: Some(Box::new(BinaryTree{ data: 2, left: None, right: None })),
            right: None,
        };
        assert_eq!(expected, t);
    }
    #[test]
    fn insert_right() {
        let mut t = BinaryTree::new(4);
        t.insert(6);

        let expected = BinaryTree {
            data: 4,
            left: None,
            right: Some(Box::new(BinaryTree{ data: 6, left: None, right: None })),
        };
        assert_eq!(expected, t);
    }
    #[test]
    fn insert_both() {
        let mut t = BinaryTree::new(4);
        t.insert(6);
        t.insert(2);

        let expected = BinaryTree {
            data: 4,
            left: Some(Box::new(BinaryTree{ data: 2, left: None, right: None })),
            right: Some(Box::new(BinaryTree{ data: 6, left: None, right: None })),
        };
        assert_eq!(expected, t);
    }


    #[test]
    fn rotate_left() {
        let mut t = BinaryTree::new(6);
        t.insert(4);
        t.insert(2);

        let expected = BinaryTree {
            data: 4,
            left: Some(Box::new(BinaryTree{ data: 2, left: None, right: None })),
            right: Some(Box::new(BinaryTree{ data: 6, left: None, right: None })),
        };
        assert_eq!(expected, t);
    }
    #[test]
    fn rotate_right() {
        let mut t = BinaryTree::new(2);
        t.insert(4);
        t.insert(6);

        let expected = BinaryTree {
            data: 4,
            left: Some(Box::new(BinaryTree{ data: 2, left: None, right: None })),
            right: Some(Box::new(BinaryTree{ data: 6, left: None, right: None })),
        };
        assert_eq!(expected, t);
    }

    #[test]
    fn insert_middle() {
        let mut t = BinaryTree::new(2);
        t.insert(4);
        t.insert(6);
        t.insert(3);

        let expected = BinaryTree {
            data: 4,
            left: Some(Box::new(BinaryTree{
                data: 2,
                left: None,
                right: Some(Box::new(BinaryTree{ data: 3, left: None, right: None })),
            })),
            right: Some(Box::new(BinaryTree{ data: 6, left: None, right: None })),
        };
        assert_eq!(expected, t, "expected {:#?}\nfound: {:#?}", expected, t);
    }
    #[test]
    fn insert_sub_tree() {
        let mut t = BinaryTree::new(2);
        t.insert(4);
        t.insert(6);
        // zweites Blatt
        t.insert(3);
        t.insert(1);

        let expected = BinaryTree {
            data: 4,
            left: Some(Box::new(BinaryTree{
                data: 2,
                left: Some(Box::new(BinaryTree{ data: 1, left: None, right: None })),
                right: Some(Box::new(BinaryTree{ data: 3, left: None, right: None })),
            })),
            right: Some(Box::new(BinaryTree{ data: 6, left: None, right: None })),
        };
        assert_eq!(expected, t, "expected {:#?}\nfound: {:#?}", expected, t);
    }

    #[test]
    fn iter_simple() {
        let mut t = BinaryTree::new(2);
        t.insert(4);
        t.insert(6);

        let expected = vec![2,4,6];
        let found = t.iter().collect::<Vec<isize>>();
        assert_eq!(expected, found);
    }
    #[test]
    fn iter_sub_tree() {
        let mut t = BinaryTree::new(2);
        t.insert(4);
        t.insert(6);
        // zweites Blatt
        t.insert(3);
        t.insert(1);

        let expected = vec![1,2,3,4,6];
        let found = t.iter().collect::<Vec<isize>>();
        assert_eq!(expected, found);
    }
    #[test]
    fn iter_sub_tree_both() {
        let mut t = BinaryTree::new(2);
        t.insert(4);
        t.insert(6);
        // zweites Blatt
        t.insert(3);
        t.insert(1);
        // drittes Blatt
        t.insert(5);
        t.insert(7);

        let expected = vec![1,2,3,4,5,6,7];
        let found = t.iter().collect::<Vec<isize>>();
        assert_eq!(expected, found);
    }


    #[test]
    fn contains_root() {
        let t = BinaryTree::new(2);
        assert!(t.contains(&2));
        assert!(t.contains(&3) == false);
    }
    #[test]
    fn contains_left() {
        let mut t = BinaryTree::new(2);
        t.insert(0);
        assert!(t.contains(&2));
        assert!(t.contains(&0));
        assert!(t.contains(&3) == false);
    }
    #[test]
    fn contains_right() {
        let mut t = BinaryTree::new(2);
        t.insert(4);
        assert!(t.contains(&2));
        assert!(t.contains(&4));
        assert!(t.contains(&3) == false);
    }
    #[test]
    fn contains_sub_tree() {
        let mut t = BinaryTree::new(2);
        t.insert(4);
        t.insert(6);
        // zweites Blatt
        t.insert(3);
        t.insert(1);

        assert!(t.contains(&1));
        assert!(t.contains(&2));
        assert!(t.contains(&3));
        assert!(t.contains(&4));
        assert!(t.contains(&5) == false);
        assert!(t.contains(&6));
    }

    #[test]
    fn massive_insert() {
        let mut t = BinaryTree::new(0);
        for i in 1..100 {
            t.insert(i);
        }

        assert!(t.contains(&0));
        for i in 1..100 {
            assert!(t.contains(&i));
            assert!(t.contains(&(-i)) == false, "false positive: {}", -i);
        }

        let expected: Vec<(usize, isize)> = Vec::new();
        assert_eq!(
            expected,
            t.iter()
                .enumerate()
                .fold(vec![], |mut v, (i, x)| {
                    if i != x as usize {
                        v.push((i, x));
                    }
                    v
                })
        );
    }

    #[test]
    fn collect_from_vec() {
        let expected: Vec<isize> = vec![4,6,8,10,12];
        let mut t: BinaryTreeCollection = expected.iter().cloned().collect();

        assert_eq!(expected, t.iter().collect::<Vec<isize>>());
    }
}
